-=-=-=-=-=-=-=-=-=-=-=- Andrei Bogdan Alexandru -=-=-=-=-=-=-=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 336 CB -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Tema 3 APD -=-=-=-=-=-=-=-=-=-=-=-=-=-=

Pentru implementare vom descrie comportamentul a 3 categorii de noduri in
cadrul rularii programului si a fiecarei functii : Radacina, Intermediar si
Frunza.

~ Main ~
Radacina va coordona intreg procesul, trimitand date catre nodurile din
topologie pentru a le controla.

--- read_topologie ---
Toate nodurile citesc din fisierul topologie pana cand isi gasesc linia ce
descrie lista lor de vecini. Din linia gasita sunt extrasi toti vecinii si
salvati intr-un vectorul de copii (parintele va fi eliminat mai tarziu)

--- whos_your_daddy ---
Fiecare nod va trimite mesaj catre fiecare copil pentru ca acestia sa isi
salveze parintele (separat de lista de parinti). Nodurile Intermediare si
Frunzele vor avea Recv, sursa fiind parintele. In momentul in care este
stabilit, acesta este scos din lista de copii si este salvat separat.

--- read_img ---
Radacina va folosi aceasta functie pentru a citi o imagine (matricea de
pixeli in principiu)

--- trimite_filtru ---
Radacina stabileste id-ul filtrului folosit (sobel = 1, mean_removal = 2)
si il trimite catre copii.
Fiecare Intermediar si Frunza pastreaza id-ul.
Intermediarii trimit mai departe id-ul.

--- trimite_dimensiune ---
Radacina va citi din fiecare poza dimensiunile width si height, dupa le va
transmite catre fiecare copil.
! Ultimul copil va primi mereu numarul ramas de linii alocate, height calculat
si trimis separat.
Fiecare Intermediar si Frunza va primi aceste dimensiuni pe care si le salveaza.
Intermediarii vor trimite mai departe catre copii dimensiunile recalculate,
folosind aceeasi regula cu Radacina.

--- trimite_pixeli ---
Toate nodurile cunosc dimensiunile pe care le asteapta de la Parinti.
Radacina va citi acum matricea de pixeli a fotografiei si va trimite acum
copiiilor liniile asociate.
Intermediarii primesc date si le vor trimite mai departe catre copii, dupa
aceeasi regula cu a parintilor.

~ Main (Frunze) ~ 
Frunzele vor aplica filtrul stabilit pe matricea de pixeli primita.

--- aduna_pixeli ---
Frunzele trimit pixelii catre parinti.
Intermediarii aduna datele primite si trimit mai departe catre parinti 
Radacina va centraliza toate datele primite si va scrie noua matrice de pixeli
in fisierul output.

--- we_are_leaving ---
Radacina va trimite un mesaj catre toate nodurile o valoare 0 daca inca mai
avem de procesat sau 1 daca am terminat.

--- wait_for_stats & send_stats ---
Frunzele vor trimite numarul de linii rezolvate catre parinti.
Intermediarii vor trimite statistica lor (0) si vor face forward catre parinti
tuturor mesajelor venite de la copii.
Radacina va primi aceste mesaje, salvand pentru fiecare nod statistica.

Cand Radacina primeste statistica din partea tuturor nodurilor din topologie,
trimite un mesaj prin care anunta toate nodurile ca vom iesi.
Intermediarii trimit mai departe semnalul si isi va incheia executia.
Frunzele se vor opri fara a mai efectua si alte operatii.

~ Main (Radacina) ~
Radacina va scrie fisierul de statistica si isi va incheia executia.
