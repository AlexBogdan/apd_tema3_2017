#include <mpi.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

/**
 mpirun -np N ./filtru topologie.in imagini.in statistica.out
**/

using namespace std;

// ~~~~~~ Filtru Sobel ~~~~~~~~~
int sobel_matrix[3][3] = {1, 0, -1,
						  2, 0, -2,
						  1, 0, -1};
int sobel_factor = 1;
int sobel_deplasament = 127;

// ~~~~~~ Filtru MeanRemoval ~~~~~~~~
int mean_matrix[3][3] = {-1, -1, -1,
						 -1,  9, -1,
						 -1, -1, -1};
int mean_factor = 1;
int mean_deplasament = 0;

/*
	Formeaza topologia pentru fiecare nod.
*/
void read_topologie(char *filename, int rank, vector<int> &children) {

	ifstream in(filename);
	string line;

	// Sarim liniile care nu corespund cu ID-ul procesului care citeste din fisier
	while (getline(in, line)) {
		int dest = stoi(line.substr(0, line.find(":")));
		if (dest == rank) {
			break;
		}
	}

	// Salvam vecinii in vectorul de copii (vol elimina mai tarziu parintele)
	istringstream iss(line.substr(line.find(":")+2, line.length() -1));
 	int number;
	while (iss >> number) {
		children.push_back(number);
	}

	return;
}

/* 
	Stabilim cine este parintele pentru fiecare nod din topologie
*/
void whos_your_daddy(int rank, vector<int> *children, int *parrent) {
	int tag = 999999, msg = 0;
	if(rank != 0) {
		MPI_Status status;
		MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		*parrent = status.MPI_SOURCE;

		// Stergem parintele din lista de copii (vecini)
		children->erase(find(children->begin(), children->end(), *parrent));
	}

	// Trimitem mesaje catre copii
	for (auto child : *children) {
		if (child != *parrent) {
			MPI_Send(&msg, 1, MPI_INT, child, tag, MPI_COMM_WORLD);
		}
	}
}

/* 
		Citim primele linii (pana la matricea de pixeli) si scriem
	aceste linii de antet in fisierul de output
*/
void read_img(ifstream &input_img, ofstream &output_img, int &width, int &height, int **&pixels) {
	string img_line;

	getline(input_img, img_line); // ceva
	output_img << img_line << endl;

	getline(input_img, img_line); // comment
	output_img << img_line << endl;

	input_img >> width >> height; // W si H
	output_img << width << " " << height << endl;

	int numar_ciudat;
	input_img >> numar_ciudat; // 255 ?
	output_img << numar_ciudat << endl;
	
	pixels = new int*[height+2];
	for (int i = 0; i < height+2; i++) {
		pixels[i] = new int[width+2];
	}
	// Bordam matricea de pixeli cu 0
	for (int i = 0; i <= width+1; i++) {
		pixels[0][i] = 0;
		pixels[height+1][i] = 0;
	}
	for (int i = 0; i <= height+1; i++) {
		pixels[i][0] = 0;
		pixels[i][width+1] = 0;
	}
	// Citim matricea de pixeli
	for (int i = 1; i <= height; i++) {
		for (int j = 1; j <= width; j++) {
			input_img >> pixels[i][j];
		}
	}
}

int stabilire_filtru(string filtru) {
	cout << filtru << " ";
	if (filtru.compare("sobel") == 0) {
		return 1;
	}
	if (filtru.compare("mean_removal") == 0) {
		return 2;
	}

	return -1;
}

/*
	Anuntam toate procesele ce filtru vor folosi
	1 - Sobel | 2 - MeanRemoval | 12 - Ambele
*/
void trimite_filtru(int rank, int *id_filtru, int parrent, vector<int> &children) {
	int tag = 999998;
	if(rank != 0) {
		MPI_Recv(id_filtru, 1, MPI_INT, parrent, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	// Trimitem mesaje catre copii
	for (auto child : children) {
		MPI_Send(id_filtru, 1, MPI_INT, child, tag, MPI_COMM_WORLD);
	}
}

/*
	Fiecare nod va primi width si height pe care le va astepta de la nodurile parinte.
*/
void trimite_dimensiune(int rank, int *width, int *height, int parrent, vector<int> &children) {
	int w_tag = 999996, h_tag = 999997;
	int *size_h = new int;

	if(rank != 0) {
		MPI_Recv(width, 1, MPI_INT, parrent, w_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(height, 1, MPI_INT, parrent, h_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	if (! children.empty()) {
		*size_h = *height / children.size();
	} else {
		return ;
	}

	for (int i = 0; i < children.size() -1; i++) {
		MPI_Send(width, 1, MPI_INT, children[i], w_tag, MPI_COMM_WORLD);
		MPI_Send(size_h, 1, MPI_INT, children[i], h_tag, MPI_COMM_WORLD);
	}
	// Ultimul nod va primi height-ul ramas
	if (! children.empty()) {
		MPI_Send(width, 1, MPI_INT, children[children.size()-1], w_tag, MPI_COMM_WORLD);
		int remain_h = *height - *size_h*(children.size()-1);
		MPI_Send(&remain_h, 1, MPI_INT, children[children.size()-1], h_tag, MPI_COMM_WORLD);
	}
}

/*
	Trimite pixelii de la radacina la frunze pentru a incepe procesarea
*/
void trimite_pixeli(int rank, int parrent, int **&pixels, int width, int height, vector<int> children) {
	int send_pixels_tag = 999995;
	int size_h;
	if(rank != 0) {
		for (int i=0; i <= height+1; i++) {
			MPI_Recv(pixels[i], width+2, MPI_INT, parrent, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	}

	if (! children.empty()) {
		size_h = height / children.size();
	} else {
		return ;
	}

	for (int i = 0; i < children.size() -1; i++) {
		for (int j = size_h*i; j <= size_h*(i+1) +1; j++) {
			MPI_Send(pixels[j], width+2, MPI_INT, children[i], j - size_h*i, MPI_COMM_WORLD);
		}
	}

	// Ultimul nod va primi height-ul ramas
	if (! children.empty()) {
		int last_child = children.size()-1;

		for (int j = size_h*last_child; j <= height+1; j++) {
			MPI_Send(pixels[j], width+2, MPI_INT, children[last_child], j - size_h*last_child, MPI_COMM_WORLD);
		}
	}
}

/*
	Fiecare nod aduna pixelii de la nodurile copil.
	Rezultatul strans merge astfel spre radacina.
*/
void aduna_pixeli(int rank, int parrent, int **&pixels, int width, int height, vector<int> children) {
	int pixels_result_tag = 999994;
	
	if (! children.empty()) {
		int size_h = height / children.size();
		for (int i = 0; i < children.size() -1; i++) {
			for (int j = 1 + i*size_h; j <= (i+1)*size_h; j++) {
				MPI_Recv(pixels[j], width+2, MPI_INT, children[i], j - i*size_h , MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
			
		}
		// Ultimul nod va primi height-ul ramas
		if (! children.empty()) {
			int last_child = children.size()-1;
			for (int j = size_h*last_child+1; j <= height; j++) {
				MPI_Recv(pixels[j], width+2, MPI_INT, children[last_child],
					j - size_h*last_child , MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
	}

	if (rank != 0) {
		for (int i = 1; i <= height; i++) {
			MPI_Send(pixels[i], width+2, MPI_INT, parrent, i, MPI_COMM_WORLD);
		}
	}
}

/*
	Printeaza imaginea curenta, dupa aplicarea filtrului
*/
void print_img(ofstream &output_img, int width, int height, int **&result) {
	for (int i = 1; i <= height; i++) {
		for (int j = 1; j <= width; j++) {
			output_img << result[i][j] << "\n";
		}
	}
}

/*
	Trimitem mesajele de terminare
*/
void we_are_leaving(int rank, int parrent, vector<int> &children, int *status) {
	int end_tag = 999993;

	if (rank != 0) {
		MPI_Recv(status, 1, MPI_INT, parrent, end_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	for (auto child : children) {
		MPI_Send(status, 1, MPI_INT, child, end_tag, MPI_COMM_WORLD);
	}
}

bool is_child(int rank, vector<int> &children) {
	for (auto child : children) {
		if (rank == child) {
			return true;
		}
	}
	return false;
}

void wait_for_stats(int rank, int nProcesses, vector<int> &children, vector<int> &stats) {
	bool ok;
	int *value = new int;
	MPI_Status status;
	for (int i = 1; i < nProcesses; i++) {
		MPI_Recv(value, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		if (! is_child(status.MPI_SOURCE, children)) {
			cout << "EROARE" << endl;
			return ;
		}
		stats[status.MPI_TAG] = *value;
	}

	// Am primit toata statistica, anuntam ca vom inchide toate procesele definitiv
	for (auto child : children) {
		MPI_Send(value, 1, MPI_INT, child, 666666, MPI_COMM_WORLD);
	}
}

/*
	Frunzele vor trimite statistica catre radacina
*/
void send_stats(int rank, int parrent, vector<int> &children, int stats) {
	int final_tag = 666666;
	if (! children.empty()) {
		stats = 0;
	}
	MPI_Send(&stats, 1, MPI_INT, parrent, rank, MPI_COMM_WORLD);

	MPI_Status status;
	while (true) {
		MPI_Recv(&stats, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		if (status.MPI_SOURCE == parrent && status.MPI_TAG == final_tag) {
			for (auto child : children) {
				MPI_Send(&stats, 1, MPI_INT, child, 666666, MPI_COMM_WORLD);
			}
			return;
		}
		if (! is_child(status.MPI_SOURCE, children)) {
			cout << "EROARE" << endl;
			return ;
		}
		MPI_Send(&stats, 1, MPI_INT, parrent, status.MPI_TAG, MPI_COMM_WORLD);
	}
}

void print_topologie(int rank, int parrent, vector<int> &children) {
	/* 
		Afisare topologie pentru fiecare nod
	*/
	cout << "Sunt procesul [" << rank << "]. Parintele meu este [" << parrent << "] iar copiii mei sunt [";
	for (auto child : children) {
		cout << child << " ";
	}
	cout << "]" << endl;
}

void allocate(int **&pointer, int width, int height) {
	pointer = new int*[height+2];
	for (int i = 0; i < height+2; i++) {
		pointer[i] = new int[width+2];
	}
}

void deallocate(int **&pointer, int width, int height) {
	for (int i = 0; i < height+2; i++) {
		delete[] pointer[i];
	}
	delete[] pointer;
}

int main(int argc, char * argv[]) {
	int rank;
	int nProcesses;
	int v;

	MPI_Init(&argc, &argv);
	MPI_Status stat;

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);

	int parrent = -100000;
	vector<int> children;

	// Citim lista de vecini pentru fiecare proces
	read_topologie(argv[1], rank, children);

	// Fiecare proces va primi un mesaj de la parinte
	whos_your_daddy(rank, &children, &parrent);

	// Incepem procesarea imaginilor
	int id_filtru;
	if (rank == 0) {
		ifstream imagini(argv[2]);

		int no_of_photos;
		imagini >> no_of_photos;

		// Citim  linie cu linie din fisierul de imagini si procesam fiecare imagine
		int *status = new int;
		string filtru, source, output;
		for (int i = 0; i < no_of_photos; i++) {
			imagini >> filtru >> source >> output;
			int width, height;
			int **pixels;
			ifstream input_img(source);
			ofstream output_img(output);

			// Citim pixelii unei poze
			read_img(input_img, output_img, width, height, pixels);

			// Anunta fiecare proces ce filtru vom folosi
			id_filtru = stabilire_filtru(filtru);
			trimite_filtru(rank, &id_filtru, parrent, children);

			// Anuntam fiecare proces cate linii (height) va primi (si width)
			trimite_dimensiune(rank, &width, &height, parrent, children);

			// Trimitem fiecarui nod o portiune din matricea de pixeli
			trimite_pixeli(rank, parrent, pixels, width, height, children);

			int **result = new int*[height+2];
			for (int i = 0; i < height+2; i++) {
				result[i] = new int[width+2];
			}

			// Strangem pixelii de la toate procesele din topologie
			aduna_pixeli(rank, parrent, result, width, height, children);

			// Printam pixelii obtinuti in noua fotografie
			print_img(output_img, width, height, result);

			// Eliberam memoria rezervata pentr editarea pozei curente
			deallocate(pixels, width, height);
			deallocate(result, width, height);

			// 	 Daca am rezolvat toate fotografiile, anuntam toate procesele
			// sa trimita statistica (1 pentru DA)
			if (i == no_of_photos - 1) {
				*status = 1;
				we_are_leaving(rank, parrent, children, status);
				break;
			} else {
				*status = 0;
				we_are_leaving(rank, parrent, children, status);
			}
		}
		// Asteptam statistica din partea nodurilor din topologie
		vector<int> statistics(nProcesses, 0);
		wait_for_stats(rank, nProcesses, children, statistics);

		// Scriem statistica
		ofstream stats_file(argv[3]);
		for (int i = 0; i < statistics.size(); i++) {
			stats_file << i << ": " << statistics[i] << endl;
		}
	} else {
		int width, height; // Cat va primi fiecare proces
		int stats = 0; // Cate linii a procesat fiecare proces
		while (true) {
			// Primim id-ul filtrului pe care il vom folosi
			trimite_filtru(rank, &id_filtru, parrent, children);
			// Primim width si height pentru procesul actual
			trimite_dimensiune(rank, &width, &height, parrent, children);

			int **pixels = new int*[height+2];
			for (int i = 0; i < height+2; i++) {
				pixels[i] = new int[width+2];
			}

			// Primim matricea de pixeli destinata procesului curent
			trimite_pixeli(rank, parrent, pixels, width, height, children);

			int **result = new int*[height+2];
			for (int i = 0; i < height+2; i++) {
				result[i] = new int[width+2];
			}

			// Daca nodul este frunza, aplicam filtrul cerut pe matricea primita
			if (children.empty()) {
				int sum;
				for (int i = 1; i <= height; i++) {
					for (int j = 1; j <= width; j++) {
						sum = 0;
						if (id_filtru == 1) {
							sum += pixels[i-1][j-1] * sobel_matrix[0][0];
							sum += pixels[i-1][j] * sobel_matrix[0][1];
							sum += pixels[i-1][j+1] * sobel_matrix[0][2];

							sum += pixels[i][j-1] * sobel_matrix[1][0];
							sum += pixels[i][j] * sobel_matrix[1][1];
							sum += pixels[i][j+1] * sobel_matrix[1][2];

							sum += pixels[i+1][j-1] * sobel_matrix[2][0];
							sum += pixels[i+1][j] * sobel_matrix[2][1];
							sum += pixels[i+1][j+1] * sobel_matrix[2][2];

							sum /= sobel_factor;
							sum += sobel_deplasament;
						}

						if (id_filtru == 2) {
							sum += pixels[i-1][j-1] * mean_matrix[0][0];
							sum += pixels[i-1][j] * mean_matrix[0][1];
							sum += pixels[i-1][j+1] * mean_matrix[0][2];

							sum += pixels[i][j-1] * mean_matrix[1][0];
							sum += pixels[i][j] * mean_matrix[1][1];
							sum += pixels[i][j+1] * mean_matrix[1][2];

							sum += pixels[i+1][j-1] * mean_matrix[2][0];
							sum += pixels[i+1][j] * mean_matrix[2][1];
							sum += pixels[i+1][j+1] * mean_matrix[2][2];

							sum /= mean_factor;
							sum += mean_deplasament;
						}

						if (sum < 0) {
							sum = 0;
						}
						else if (sum > 255) {
							sum = 255;
						}
						result[i][j] = sum;
					}
				}
				stats += height;
			}
			
			// Trimitem pixelii catre radacina prin intermediul parintilor
			aduna_pixeli(rank, parrent, result, width, height, children);

			// Eliberam memoria folosita pentru procesarea imaginii
			deallocate(pixels, width, height);
			deallocate(result, width, height);

			// Anuntam incheierea programului
			int *status = new int;
			we_are_leaving(rank, parrent, children, status);
			if (*status == 1) {
				send_stats(rank, parrent, children, stats);
				break;
			}
		}
	}

	MPI_Finalize();
	return 0;
}
